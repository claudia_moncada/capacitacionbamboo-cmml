#IF %1 echo rolling back

#IF NOT %1 echo not rolling back


if $1; then 
	echo 'rolling back'
fi

if !$1; then
	echo 'not rolling back'
fi